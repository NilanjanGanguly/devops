public with sharing class lwcAccountController {
    
   
    @AuraEnabled(cacheable=true)
    public static List<Account> getAccounts()
    {
        return [select Id,Name,Description from Account LIMIT 10];
    }
}
