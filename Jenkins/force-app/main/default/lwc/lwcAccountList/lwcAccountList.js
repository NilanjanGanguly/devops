import { LightningElement,  wire } from 'lwc';
 
import getAccounts from '@salesforce/apex/lwcAccountController.getAccounts';
 
export default class CollectionExample extends LightningElement {
    @wire(getAccounts) accounts; 
}